﻿using UnityEngine;
using System.Collections;

public class Shooter : MonoBehaviour {
	public GameObject bullet;
	public Transform bulletSpawn;

	// Use this for initialization
	void Start () {
		
	}

	void Update(){
		if (Input.GetKeyDown(KeyCode.Space))
		{	
			Debug.Log("SHOOT");
			Shoot();
		}
	}
	public void Shoot(){
		GameObject bullet = (GameObject)Instantiate (
			this.bullet,
			this.bulletSpawn.position,
			this.bulletSpawn.rotation);

		// Add velocity to the bullet
		bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * 150;

		// Destroy the bullet after 2 seconds
		Destroy(bullet, 2.0f);

	}
}
