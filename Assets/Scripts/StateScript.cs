﻿using UnityEngine;
using Pathfinding;
using System.Collections;

public class StateScript : MonoBehaviour {

    public float vision;
    public float searchHealth;

	// Use this for initialization
	void Start () {
        InvokeRepeating("NextAction", 0, 0.3f);
	}
	
	// Update is called once per frame
	void Update () {
	}

    GameObject[] FindClosestEnemy() {
        Debug.Log("enemy");
        GameObject[] gos;
        gos = GameObject.FindGameObjectsWithTag("Player");
        GameObject closest = null;
        float distance = Mathf.Infinity;
        Vector3 position = this.transform.position;
        foreach (GameObject go in gos) {
            if (position != go.transform.position) {
                Vector3 diff = go.transform.position - position;
                float curDistance = diff.sqrMagnitude;
                if (curDistance < distance) {
                    closest = go;
                    distance = curDistance;
                }
            }
        }
        if (closest == null) {
            return new GameObject[0];
        }
        GameObject[] result = new GameObject[1];
        result[0] = closest;
        return result;
    }

    GameObject[] FindClosestPickup() {
        Debug.Log("pickup");
        GameObject[] gos;
        gos = GameObject.FindGameObjectsWithTag("Pickup");
        GameObject closest = null;
        float distance = Mathf.Infinity;
        Vector3 position = transform.position;
        foreach (GameObject go in gos) {
            Vector3 diff = go.transform.position - position;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance) {
                closest = go;
                distance = curDistance;
            }
        }
        if (closest == null) {
            return new GameObject[0];
        }
        GameObject[] result = new GameObject[1];
        result[0] = closest;
        return result;
    }

    GameObject[] NextTarget() {
        GameObject[] enemies = FindClosestEnemy();
        GameObject[] pickups = FindClosestPickup();
        if ((pickups.Length == 0) && (enemies.Length == 0)) {
            return new GameObject[0];
        }
        if (pickups.Length == 0) {
            return enemies;
        }
        if (enemies.Length == 0) {
            return pickups;
        }
        GameObject enemy = enemies[0];
        GameObject pickup = pickups[0];
        Vector3 diffEnemy = this.transform.position - pickup.transform.position;
        Vector3 diffPickup = enemy.transform.position - pickup.transform.position;
        float enemyDistance = diffEnemy.sqrMagnitude;
        float pickupDistance = diffPickup.sqrMagnitude;
        if (
            (enemyDistance < pickupDistance) &&
            (this.GetComponent<StatusScript>().getHealth() > searchHealth)
        ) {
            return enemies;
        }
        return pickups;
    }

    void Attack(GameObject target) {
        this.GetComponent<MineBotAI>().target = target.transform;
        this.GetComponent<MineBotAI>().range = 10.0f;
        this.GetComponent<Shooter>().bulletSpawn.transform.LookAt(target.transform);
        this.GetComponent<Shooter>().Shoot();
    }

    void Pickup(GameObject target) {
        this.GetComponent<MineBotAI>().range = 0f;
        this.GetComponent<MineBotAI>().target = target.transform;
    }

    void Search () {
        Debug.Log("Search");
        GameObject new_pos = new GameObject();
        new_pos.transform.position.Set(this.transform.position.x + Random.Range(-2, 2), 0, this.transform.position.z + Random.Range(-2, 2));
        this.GetComponent<MineBotAI>().target = new_pos.transform;
    }

    void NextAction() {
        GameObject[] targets = NextTarget();
        if (targets.Length == 0) {
            Search();
            return;
        }
        GameObject target = targets[0];
        float distance = Vector3.Distance(this.transform.position, target.transform.position);
        if (distance < vision) {
            if (target.tag == "Pickup") {
                Pickup(target);
            }
            else if (target.tag == "Player") {
                Attack(target);
            }
            else {
                Search();
            }
        }
        else {
            Search();
        }
    }
}