﻿using UnityEngine;
using System.Collections;

public class StatusScript : MonoBehaviour {
	private float health = 100;
	public GameObject HUD; 				// The Text mesh
	public Camera cam;

	// Use this for initialization
	void Start () {
	
	}

	public void lowerHealth(){
		if (this.health > 0) {
            if (this.GetComponent<PlayerMovement>() != null) {
                this.GetComponent<PlayerMovement>().PlayHit();
            }
			this.health -= 10;
			this.HUD.GetComponent<TextMesh> ().text = "HEALTH:" + this.health.ToString ();
		}
        else
        {
            if (this.GetComponent<PlayerMovement>() != null)
            {
                Application.LoadLevel("EndScene");
            }
        }
	}
	public void addHealth(){
		this.health += 20;
        if (this.GetComponent<PlayerMovement>() != null)
        {
            this.GetComponent<PlayerMovement>().PlayHealth();
        }
		this.HUD.GetComponent<TextMesh> ().text = "HEALTH:" + this.health.ToString ();
	}
	public float getHealth(){
		return this.health;
	}

	// Update is called once per frame
	void Update () {
		HUD.transform.LookAt (cam.transform);
		Vector3 ve = cam.transform.position - HUD.transform.position;
		ve.x = ve.z = 0.0f;
		HUD.transform.LookAt( cam.transform.position - ve ); 
		HUD.transform.Rotate(0,180,0);
	}
}
