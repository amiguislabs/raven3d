﻿using UnityEngine;
using System.Collections;

public class DieScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (this.GetComponent<StatusScript> ().getHealth () <= 0) {
			Destroy (this.gameObject);
		}
	}
}
