﻿using UnityEngine;
using System.Collections;

public class EndSceneScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
        StartCoroutine(MainAgain());
	}
	
	// Update is called once per frame
	void Update () {
        
	}

    IEnumerator MainAgain() {
        yield return new WaitForSeconds(5);
        Application.LoadLevel("Main");
    }

}
